<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use App\Models\JobOpportunity;

class JobsController extends Controller
{

    const JOBS_BLADE = 'jobs';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public  function index()
    {

        $selectedJob = null;
        $jobs = JobOpportunity::all();

        return view(JobsController::JOBS_BLADE, [
            "selectedJob" => $selectedJob,
            "jobs" => $jobs
        ]);
    }

    public function save(Request $request)
    {

        $data = $request->validate([
            'position' => 'required|max:255',
            'job_type' => 'required|max:255',
            'location' => 'required|max:255',
            'experience_level' => 'required|max:255',
            'experience_years' => 'required|max:255',
            'minimum_qualification' => 'required|max:255',
            'job_summary' => 'required|max:255',
            'job_description' => 'required|max:255',
            'expiry_date' => 'required|max:255'
        ]);

        $data['responsibilities'] = $request->responsibilities;
        $data['required_experience'] = $request->required_experience;
        $data['application_instructions'] = $request->application_instructions;

        $selected = $request->selected_job;

        if ($selected) {
            tap(JobOpportunity::where(
                'id',
                $selected
            )->update($data));
        } else {
            tap(new JobOpportunity($data))->save();
        }

        Session::flash('success', 'Job details saved successfully!');
        return redirect()->back();
    }

    public function search(Request $request)
    {

        $keyword = $request->keyword;
        $jobs = DB::select(Db::raw("select * from job_opportunities where position
             like '%" . $keyword . "%' or job_type like '%" . $keyword . "%'
              or location like '%" . $keyword . "%'  or minimum_qualification
               like '%" . $keyword . "%'  or experience_level like '%" . $keyword . "%'"));
        return json_encode($jobs);
    }

    public function edit($id)
    {

        $selectedJob = JobOpportunity::find($id);
        $jobs = JobOpportunity::all();

        return view(
            JobsController::JOBS_BLADE,
            [
                'jobs' => $jobs,
                'selectedJob' => $selectedJob
            ]
        );
    }

    public function del(Request $request)
    {

        $jobId = $request->deletejob;
        $job = JobOpportunity::find($jobId);
        $job->delete();

        Session::flash('success', 'Job deleted successfully!');
        return redirect()->back();
    }
}
