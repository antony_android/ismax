<?php

namespace App\Http\Controllers;

use App\Mail\RequestCertificate;
use App\Mail\RequestQuote;
use App\Mail\SendMessage;
use App\Models\JobOpportunity;
use Illuminate\Http\Request;
use App\Models\Page;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

  public  function index()
  {

    return view('home');
  }

  public function  page($slug)
  {
    $page = Page::Where('slug', $slug)->first();

    if (!empty($page)) {

      return  view('page', compact('page'));
    } else {

      abort(404);
    }
  }

  public  function contact_us()
  {
    return view('contact_us');
  }

  public  function about_us()
  {
    return view('about_us');
  }

  public  function career()
  {

    $jobs = JobOpportunity::where(
      'expiry_date',
      '>=',
      date('Y-m-d H:i:s')
    )->get();

    return view('careers', ["jobs" => $jobs]);
  }

  public  function guards()
  {
    return view('guards');
  }

  public  function consultancy()
  {
    return view('consultancy');
  }

  public  function alarms()
  {
    return view('alarms');
  }

  public  function risk()
  {
    return view('risk');
  }

  public  function events()
  {
    return view('events');
  }

  public  function cctv()
  {
    return view('cctv');
  }

  public function otherServices()
  {

    return view('other_services');
  }

  public  function clients()
  {
    return view('clients');
  }

  public function quote(Request $request)
  {

    $data = $request->validate([
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'interest' => 'required|max:255',
      'comments' => 'required',
    ]);

    $data["phone"] = $request->phone;
    $data["email"] = $request->email;
    $data["business_name"] = $request->business_name;

    Mail::to("info@ismaxsecurity.com")
      ->send(new RequestQuote($data));

    Mail::to("antonjoro2008@gmail.com")
      ->send(new RequestQuote($data));

    Mail::to("accounts@ismaxsecurity.com")
      ->send(new RequestQuote($data));

    Session::flash('success', 'Your message has been sent successfully to us! Please wait and we will be in touch. Thank you.');
    return redirect()->back();
  }

  public function message(Request $request)
  {

    $data = $request->validate([
      'first_name' => 'required|max:255',
      'last_name' => 'required|max:255',
      'interest' => 'required|max:255',
      'comments' => 'required',
    ]);

    $data["phone"] = $request->phone;
    $data["email"] = $request->email;
    $data["business_name"] = $request->business_name;


    Mail::to("info@ismaxsecurity.com")
      ->send(new SendMessage($data));

    Mail::to("antonjoro2008@gmail.com")
      ->send(new SendMessage($data));

    Mail::to("accounts@ismaxsecurity.com")
      ->send(new SendMessage($data));

    Session::flash('success', 'Your message has been sent successfully to us! Please wait and we will be in touch. Thank you.');
    return redirect()->back();
  }

  public function certificate(Request $request)
  {

    $data = $request->validate([
      'full_name' => 'required|max:255',
      'id_no' => 'required|numeric',
      'phone' => 'required|max:255',
      'doe' => 'required|max:20',
    ]);

    $data["last_doe"] = $request->last_doe;
    $data["reason"] = $request->reason;
    $data["comments"] = $request->comments;

    Mail::to("info@ismaxsecurity.com")
      ->send(new RequestCertificate($data));

    Mail::to("antonjoro2008@gmail.com")
      ->send(new RequestCertificate($data));

    Mail::to("accounts@ismaxsecurity.com")
      ->send(new RequestCertificate($data));

    Session::flash('success', 'Your request has been sent successfully to us! Please wait and we will be in touch. Thank you.');
    return redirect()->back();
  }
}
