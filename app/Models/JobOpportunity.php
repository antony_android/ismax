<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $position
 * @property integer $job_type
 * @property string $location
 * @property string $minimum_qualification
 * @property string $experience_level
 * @property integer $experience_years
 * @property string $job_summary
 * @property string $job_description
 * @property string $responsibilities
 * @property string $required_experience
 * @property string $application_instructions
 * @property string expiry_date
 * @property integer $status
 * @property string $created
 * @property string $modified
 */
class JobOpportunity extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'job_opportunities';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'position', 'job_type', 'location', 'minimum_qualification',
        'experience_level', 'experience_years', 'job_summary',
        'job_description', 'responsibilities', 'expiry_date',
        'required_experience', 'application_instructions',
        'status', 'created', 'modified'
    ];
}
