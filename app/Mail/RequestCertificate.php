<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RequestCertificate extends Mailable
{
    use Queueable, SerializesModels;


    public $fullName;
    public $idNo;
    public $doe;
    public $phone;
    public $lastDoe;
    public $reason;
    public $comments;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $data
    ) {
        $this->fullName = $data["full_name"];
        $this->idNo = $data["id_no"];
        $this->doe = $data['doe'];
        $this->lastDoe = $data['last_doe'];
        $this->phone = $data['phone'];
        $this->reason = $data['reason'];
        $this->comments = $data['comments'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('webmaster@ismaxsecurity.com')
            ->subject('Message from Website User - Request for Certificate')
            ->view('emails.certificate');
    }
}
