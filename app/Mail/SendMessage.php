<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessage extends Mailable
{
    use Queueable, SerializesModels;


    public $firstName;
    public $lastName;
    public $businessName;
    public $emailAddress;
    public $phone;
    public $interest;
    public $comments;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        $data
    ) {
        $this->firstName = $data["first_name"];
        $this->lastName = $data["last_name"];
        $this->businessName = $data['business_name'];
        $this->emailAddress = $data['email'];
        $this->phone = $data['phone'];
        $this->interest = $data['interest'];
        $this->comments = $data['comments'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('webmaster@ismaxsecurity.com')
            ->subject('Message from Website User - Contact Us')
            ->view('emails.message');
    }
}
