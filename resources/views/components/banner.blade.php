<section id="banner">

  <!-- Slider -->
  <div id="main-slider" class="flexslider">
    <ul class="slides">

      <li>
        <img src="img/slides/on-site.jpg" alt="" />
        <div class="flex-caption">
          <h3>On Site Security</h3>
          <p>Whеn іt’ѕ іmроrtаnt tо hаvе unіfоrmеd ѕесurіtу оffісеrѕ оnѕіtе tо ѕесurе уоur рrореrtу, уоu саn reliably trust Ismax Security</p>
          <a href="/contact" class="header-bg-button">CONTACT US</a>
        </div>
      </li>
      <li>
        <img src="img/slides/3.jpg" alt="" />
        <div class="flex-caption">
          <h3>V.I.P Protection</h3>
          <p>Ismax Security understands V.I.Ps protection. With our specially trained officers, we offer unmatched V.I.P protection.</p>
          <a href="/contact" class="header-bg-button">CONTACT US</a>
        </div>
      </li>
      <li>
        <img src="img/slides/guards.png" alt="" />
        <div class="flex-caption">
          <h3>V.I.P Protection</h3>
          <p>Ismax Security understands V.I.Ps protection. With our specially trained officers, we offer unmatched V.I.P protection.</p>
          <a href="/contact" class="header-bg-button">CONTACT US</a>
        </div>
      </li>
      <li>
        <img src="img/slides/cctv-1.jpg" alt="" />
        <div class="flex-caption">
          <h3>V.I.P Protection</h3>
          <p>Ismax Security understands V.I.Ps protection. With our specially trained officers, we offer unmatched V.I.P protection.</p>
          <a href="/contact" class="header-bg-button">CONTACT US</a>
        </div>
      </li>
    </ul>

  </div>
  <!-- end slider -->
</section>