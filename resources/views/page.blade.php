	@extends('layouts.app')

	<!-- Carousel section -->

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 ">
					<h1 class="pageTitle">{!! $page->title!!}</h1>
				</div>
				<div class="overflow-sec primary-color">

				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class=" page-info">
								<!-- <img src="{!! url('storage/'.$page->image)!!}" 
									width="100%" height="300px" > -->
								{!! $page->body !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@endsection