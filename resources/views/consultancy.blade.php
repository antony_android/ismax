	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="pageTitle">SECURITY CONSULTANCY</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-12">
										<img src="/img/consult.jpg" class="services-img" alt="Guards" />
										<div>
											<p>We can help you create a robust security environment with services that include threat assessments, policy review and development, and master planning.</p>
											<p>Security decisions you make today can determine your organization’s security and resilience for years to come. Our comprehensive security consulting services enable you to feel more confident about the actions you take to protect your family office, employees, operations, facilities, and assets.</p>
											<p>Our security consultants have decades of experience advising private clients and corporations across industries that range from construction, manufacturing, and transportation to education, hospitality, and government. We can help you create a robust security environment with services that include current and emerging threat assessments, policy review and development, and master planning.</p>
											<p class="services-submenu">Integrated security framework for stronger risk mitigation</p>
											<p>Based on time-tested best practices as well as real-world experience, our holistic approach reflects the interrelated nature of today’s businesses. It’s a classic case where the whole is greater than the sum of its parts — an integrated security framework provides for stronger, more cohesive protection to mitigate threats to your organization.</p>
											<p>For example, there are physical, operational. By looking at your security challenges from several vantage points, a Ismax specialist can help you better prevent, plan for, and respond to threats.</p>
											<p>Our security consulting services include:</p>
											<div class="row">
												<div class="col-sm-5">

													<ul style="list-style: none;line-height: 2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Threat and vulnerability assessments</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Policy and procedure review and development</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Security audits</li>
													</ul>
												</div>
												<div class="col-sm-5">

													<ul style="list-style: none;line-height: 2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Security training</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Security master planning</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Securing intellectual property</li>
													</ul>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>

	@endsection