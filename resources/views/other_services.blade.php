	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12">
	                <h1 class="pageTitle">OTHER SERVICES</h1>
	            </div>
	            <div class="overflow-sec primary-color">
	            </div>
	        </div>
	    </div>
	</section>

	<section id="about-content">
	    <div class="container content">
	        <!-- Service Blocks -->

	        <div class="row">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/front_desk.png" class="others-img-left" alt="Front Desk" />
	                                    <div>
	                                        <p class="services-submenu">Reception Security</p>
	                                        <p>Our highly trained security officers will ensure that
	                                            any person attempting to gain access to your
	                                            premises must have your permission and only be
	                                            given access to permitted areas fora valid reason.
	                                            All visitors will be required to sign the visitor's book
	                                            and will not go in entry until a security pass is issued,
	                                            complete with the time and name of your company
	                                            members contact. They will also be signed out before
	                                            they leave your premises.
	                                            As well as taking care of security issues, where
	                                            circumstances permit and the client requests it, our
	                                            lsmax Security officers will:
	                                        <div class="row">
	                                            <div class="col-sm-6">
	                                                <ul style="list-style: none;line-height: 2;">
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Keep tabs on the visitors' log</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Help visitors with directions</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Provide support to reception staff</li>
	                                                </ul>
	                                            </div>
	                                            <div class="col-sm-6">
	                                                <ul style="list-style: none;line-height:2;">
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Escort the visitor to the concerned staff</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Handle first aid and other emergencies</li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/alarm-response.png" class="others-img-right" alt="Front Desk" />
	                                    <div>
	                                        <p class="services-submenu">Alarm Response</p>
	                                        <p>Linked to our Key Holding service, our Alarm
	                                            Response measures protect you around the clock.
	                                            As soon as our alarm receiving center gets the
	                                            signal, we will be on our way, often reaching your
	                                            premises before the Police:
	                                        <div class="row">
	                                            <div class="col-sm-12">
	                                                <ul style="list-style: none;line-height: 2;">
	                                                    <li>
	                                                        <i class="fas fa-check primary-color"></i>&nbsp;We use uniformed Response Officers and liveried vehicles
	                                                    </li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;We carry out checks and cordon the area for intruders, damage and signs of forced entry</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;We make your premises safe</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;We liaise with the Emergency Services</li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/mobile-patrols.png" class="others-img-left" alt="Front Desk" />
	                                    <div>
	                                        <p class="services-submenu">Mobile Patrols</p>
	                                        <p>Crime prevention is our priority, but we are also
	                                            quick to respond. lsmax Mobile Patrols work in a
	                                            number of ways. Assigned to your premises and
	                                            their surroundings, our patrols will keep regular
	                                            checks on even the most remote corners where
	                                            trouble can lurk</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/command-control.png" class="others-img-right" alt="Front Desk" />
	                                    <div>
	                                        <p class="services-submenu">Command Control Centre</p>
	                                        <p>Our Security Officers are closely supervised and
	                                            ably supported by their Team Leaders and
	                                            Managers Together, they have the kind of local
	                                            knowledge and on-site training that will keep them
	                                            a step ahead, whatever the threat. They also have
	                                            the back up of Ismax's Security Control Room.
	                                            Around the clock. This state-of-the-art nerve center
	                                            is at the forefront of CCTV Mechnology, supporting
	                                            Static Officers and Mobile Patrols in the field,
	                                            keeping check on Lone Workers in isolated and
	                                            often demanding workplaces - indoors and out.
	                                            Ready for the unexpected, they are also just a phone
	                                            call away from our most senior managers, ready to
	                                            handle any crisis.</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <div>
	                                        <p class="services-submenu">Retail Security</p>
	                                        <p>Many of Ismax's Security Officers have a background
	                                            working in customer-facing roles. Our uniformed
	                                            Officers are skilled at dealing with large crowds and
	                                            evacuation procedures. We also deploy covert store
	                                            detectives, oversee control room operations,
	                                            manage car parks and provide First Aid Services.
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/logistics.png" class="others-img-right" alt="Logistics" />
	                                    <div>
	                                        <p class="services-submenu">Logistics</p>
	                                        <p>As well as providing static security officers, we
	                                            also move with you. Whether transporting
	                                            freight by road and rail, our security solutions
	                                            con provide the support and infrastructure, you
	                                            need to trace track and trail valuable goods. Our
	                                            security consultants can analyse your plans,
	                                            carrying out a comprehensive safety and security
	                                            audit. We will put in place plant and personnel to
	                                            help you monitor your assets every step of the
	                                            way using sophisticated electronics. They will spot
	                                            weaknesses in your perimeter fences,
	                                            challenge suspicious traffic in and out of your
	                                            gates and Operate remote gate monitoring
	                                            systems.

	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/k9.png" class="others-img-left" alt="K9" />
	                                    <div>
	                                        <p class="services-submenu">K9 Unit</p>
	                                        <p>These K9 Units are well trained and handled by our
	                                            professional dog handlers.
	                                            They are sensitive to sniff drugs, cash notes and so
	                                            many others. Attack dogs are also in our fleet.

	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/car-park.png" class="others-img-right" alt="Car Parks" />
	                                    <div>
	                                        <p class="services-submenu">Car Parks</p>
	                                        <p>As part of your security arrangements, Ismax
	                                            Security Officers can keep watch over your
	                                            organization's car parking facilities. Officers will
	                                            carry out frequent, but deliberately random patrols
	                                            to:

	                                        <div class="row">
	                                            <div class="col-sm-12">
	                                                <ul style="list-style: none;line-height: 2;">
	                                                    <li>
	                                                        <i class="fas fa-check primary-color"></i>&nbsp;Provide a high-profile deterrent against
	                                                        thieves and vandals.
	                                                    </li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Ensure that visitors park correctly in the
	                                                        designated spaces. Assist drivers in finding
	                                                        spaces, exits and so on.</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Enforce parking rules and charges if these
	                                                        apply as issuing penalty notices Monitor the
	                                                        use of disabled parking spaces and assist
	                                                        disabled visitors.</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Should any incidents occur, our patrols will
	                                                        ensure these are dealt with, recorded and
	                                                        reported</li>
	                                                </ul>
	                                            </div>
	                                        </div>

	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <div>
	                                        <p class="services-submenu">Private Investigations</p>
	                                        <p>Statistics suggest that existing employees commit
	                                            more than 80% of corporate fraud. Most cases
	                                            are uncovered by accident, many when it is too
	                                            late. That is why we place as much emphasis on
	                                            prevention as we do on recovery.
	                                            Before trouble strikes, we will audit and examine,
	                                            ring-fence and upgrade, the re-examine -
	                                            systems, people, processes, supply chains and
	                                            more. We will provide you with specific indicators
	                                            you can monitor to spot trouble.
	                                            If what you suspect has already happened, we will
	                                            investigate - covertly and discreetly - until we
	                                            identify the source of the problem.
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>


	        <div class="row" style="margin-top: 20px;">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <div>
	                                        <p class="services-submenu">Vetting & Screening</p>
	                                        <p>Just as Officers in the security industry have to be
	                                            screened, the same applies in other professional
	                                            occupations. We can try out independent vetting
	                                            checks to various levels. Beside public sector
	                                            employees, our Security Consultants are involved
	                                            with the private sector and in very sensitive and
	                                            sensitive roles.
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>


	    </div>
	</section>

	@endsection