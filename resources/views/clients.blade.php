	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12">
	                <h1 class="pageTitle">OUR CLIENTS</h1>
	            </div>
	            <div class="overflow-sec primary-color">
	            </div>
	        </div>
	    </div>
	</section>

	<section id="about-content">
	    <div class="container content">
	        <!-- Service Blocks -->

	        <div class="row">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/thumbs-up.png" class="clients-img" alt="Happy Clients" />
	                                    <div>
	                                        <p>Our clientele includes major corporate, mid-sized
	                                            companies, non-profit organizations, prominent
	                                            individuals, government at all levels, and others.
	                                            Working in close partnership with our clients, Ismax
	                                            Security is unequaled at risk mitigation and threat
	                                            management, consultations and investigations,
	                                            corporate security, special events security, response
	                                            security services and a full range of related services. </p>

	                                        <p>By providing top notch services anchored on our guiding principles and core values,
	                                            a lot of customers have entrusted us with their security solutions. Some of our clients include:
	                                        </p>
	                                    </div>
	                                </div>
	                                <div class="row">
	                                    <div class="col-sm-7">

	                                        <ul style="list-style: none;line-height: 2;">
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Technical University of Kenya (TUK)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;United States International University (USIU)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;The Nairobi Hospital (TNH)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenyatta National Hospital (KNH)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Agriculture and Food Authority</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Property Development Management (Part of
	                                                Aga Khan Development Network)
	                                            </li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Mandera County Government</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;National Hospital Insurance Fund (NHIF)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Ethics and Anti-Corruption Commission (EACC)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenya Civil Aviation Authority (KCAA)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;First Community Bank</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenya Ferry Services</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Masinde Muliro University of Science and Technology</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenyatta University Teaching Research and Referral Hospital</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Siemens AG (Germany)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Proto Energy</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenya National Examinations Council (KNEC)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Jomo Kenyatta University of Science and Technology (JKUAT)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenya Power & Lighting Company (KPLC)</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Umma University</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Siemens S.AS Kenya</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Kenya Coast Polytechnic</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Grain Industries Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Tosha Petroleum</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Nomad Palace Hotel</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Lenana Towers</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Leeban Estate</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;IBGARO Realtors</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Alibaba Furnitures</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Trojan International Petroleum</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Cianda Market</li>
	                                        </ul>
	                                    </div>
	                                    <div class="col-sm-5">

	                                        <ul style="list-style: none;line-height: 2;">
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Treasury</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Shangai Construction Group Company</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Andalus Hospital</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Awale Pharmaceuticals</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Beyruha Academy</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Ceytun Construction Industry and Trade</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Ecomesa Shopping</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Darford Industries Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;GASCOM Petroleum Limited SHELL Ruiru</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Halai Estate South C</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;HALAL EPZ</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Hayat Hospital Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;HUDA Integrated School</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Al-Huda Mosque</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;IFTIN Money Express Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;KIP Melamine Co. Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Landmark International Properties Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Lobby Rubber (K) Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;MAAR Petroleum Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Makkah Hospital</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Mandera Shopping Mall</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Mega Wholesalers</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Olympic Petroleum</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;PASKA Construction Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Petronnas (K) Limited Shell Ngong Road</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Quality Plast Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Rajo Television Network</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;SIFA Spring Estate</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;SkyLight Chemicals Limited</li>
	                                            <li><i class="fas fa-check primary-color"></i>&nbsp;Marwa Restaurant</li>
	                                        </ul>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>


	    </div>
	</section>

	@endsection