	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="pageTitle">ALARM & FIRE SYSTEMS</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-12">
										<img src="/img/fire-alarm.jpg" class="services-img" alt="Guards" />
										<div>
											<p class="services-submenu" style="font-size: 1.2rem;">Fire alarm technology will keep your family safe, no matter what happens</p>
											<p>Through our outstanding officers performance and the large fleet, we believe that we deliver the fastest response times in the industry. Although crime prevention is important.The tasks of monitoring a facility or application for potential fire threats or operating a releasing system are facilitated by a fire detection and alarm system.</p>
											<p>Fire detection devices, typically smoke, heat or optical fire detectors, electronically relay fire events to a fire control panel which processes the fire detection signals from protected areas and immediately performs key operations, including sounding alarms, shutting down equipment, and releasing fire suppression systems. The control panel also facilitates input signals from manual stations, as well as audible and visual notification devices.</p>
											<p>These systems are supervised to ensure circuit integrity. In addition, the control panels are supplied with battery backup in the event of power loss.</p>
											<p>We offer emergency alarm response services for all properties. If your security system has been activated whilst you are away from your home or office, our trained Key Guards can provide fast response at any time, day or night. Our alarm response services include rapid response to:</p>
											<ul style="list-style: none;line-height: 2;">
												<li><i class="fas fa-check primary-color"></i>&nbsp;Alarm system activations</li>
												<li><i class="fas fa-check primary-color"></i>&nbsp;Reports of break-ins</li>
												<li><i class="fas fa-check primary-color"></i>&nbsp;Attendance requests from emergency services</li>
											</ul>
											<p>When an alarm sounds on your property, your alarm company can communicate directly with our dispatch center and an officer will immediately be dispatched to inspect your property. When there is damage or a threat to your property, an officer will secure the area and notify the proper authorities as well as property management.</p>
											<p>Each time our officers respond, they will complete a detailed report, which will be sent to you for your records. Our services will help you avoid the extra cost in fees and fines from the city and counties for responding to false alarms.</p>
											<p class="services-submenu">Patrol & Alarm Response Services</p>
											<p>In select markets throughout the country, Ismax Security provides security patrol services to enhance safety as well as to assist in the prevention, deterrence, and detection of trespassing, vandalism and criminal activity at your property. Our patrol officers are highly trained, uniformed personnel who patrol property in clearly marked vehicles. If we find evidence of or observe criminal activity, trespassing, vandalism, or simply anything out of the ordinary, our patrol officers immediately notify local law enforcement and client representatives.</p>
											<p>Ismax Security’s patrol services are customized to meet the unique needs of each client and can include randomly scheduled patrol service, regularly scheduled patrol service, response to emergency situations, temporary coverage, or scheduled services like monitoring equipment, providing escort services, or unlocking doors at specific times at your facility.</p>
											<p>Patrol officers are also available to respond to building alarms when notified by our dispatch center. After responding to the alarm, the patrol officers will notify local law enforcement and client representatives if we find evidence of criminal activity or vandalism at the property. Ismax Security’s alarm response service provides quick notification of security issues related to the alarm at your property while reducing frustration and costs associated with false alarms.</p>
											<p>Through our patrol and alarm response services, the same industry leading security service provided to major corporations throughout the country is available and affordable for small businesses.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>

	@endsection