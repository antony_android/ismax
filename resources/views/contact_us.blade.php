	@extends('layouts.app')

	<!-- Carousel section -->

	@section('content')

	<script>
		function validateEmail(email) {
			const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
		}

		function submitMessageRequest() {

			let email = $('#email').val();
			if (email) {

				if (!validateEmail(email)) {
					alert("Please enter a valid email address");
					return;
				}
			}

			document.getElementById('send-message-form').submit();

		}
	</script>

	<section id="contactsection">
		<div class="container">
			<div class="row">
				<div class="col-md-6" style="margin: auto 0;">
					<h1 class="text-light" style="margin-bottom: 2rem !important;">CONTACT DETAILS </h1>

					<p class="text-light">
						Ismax Security Limited <br>
						Ole Shapara Road, <br>
						South C <br>
						Opposite Shree Ambaji Temple <br>
						Nairobi, Kenya <br>
						Tel: +254 (0) 786 600014, 733 155512 <br>
						Email: info@ismaxsecurity.com <br>
					</p>
				</div>

				<div class="col-md-6">
					<div class="card contact-form shadow">
						<div class="card-body p-2-5">
							<h3 style="color:#EB1B23;font-size:1.4rem;"> FREE QUOTE / MESSAGE US </h3>
							<form id="send-message-form" method="POST" action="/message">
								@csrf
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group mb-3">

											<input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name" placeholder="First Name">
											@error('first_name')
											<div class="invalid-feedback">{{ $message }}</div>
											@enderror
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group mb-3">

											<input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name" placeholder="Last Name">
											@error('last_name')
											<div class="invalid-feedback">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group mb-3">

											<input type="text" class="form-control" id="business_name" name="business_name" placeholder="Business Name">

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group mb-3">

											<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group mb-3">

											<input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Email Address">
											@error('email')
											<div class="invalid-feedback">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group mb-3">

											<select class="form-control @error('interest') is-invalid @enderror" id="interest" name="interest">
												<option value="">I am Interested in</option>
												<option value="Inquiry">Sending an Inquiry</option>
												<option value="CCTV System">CCTV System</option>
												<option value="On-site Security">On-site Security / Guards</option>
												<option value="Alarm Monitoring System">Alarm Monitoring System</option>
												<option value="Life & Fire Protection System">Fire Protection System</option>
												<option value="Consultancy">Security Consultancy</option>
												<option value="Event Security">Event Security</option>
											</select>
											@error('interest')
											<div class="invalid-feedback">{{ $message }}</div>
											@enderror
										</div>
									</div>
								</div>
								<div class="form-group mb-3">

									<textarea class="form-control @error('interest') is-invalid @enderror" id="comments" name="comments" rows="3" placeholder="Comments"></textarea>
									@error('comments')
									<div class="invalid-feedback">{{ $message }}</div>
									@enderror
								</div>
								<button type="submit" class="btn btn-success btn-sm form-control py-2" onclick="event.preventDefault(); submitMessageRequest();" style="background-color: #e4222b; border-color: #e4222b;border-radius:5px !important;">Send</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="mapouter-contact">
		<div class="gmap_canvas-contacts">
			<iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=Ole%20Shapara%20Road,%20South%20C&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
			<style>
				.mapouter-contact {
					position: relative;
					text-align: right;
					height: 300px;
					width: 100%;
				}
			</style>
			<style>
				.gmap_canvas-contacts {
					overflow: hidden;
					background: none !important;
					height: 300px !important;
					width: 100% !important;
				}
			</style>
		</div>
	</div>

	@endsection