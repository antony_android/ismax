<!-- start header -->

<style type="text/css">
    .navbar-light .navbar-nav .nav-link {
        color: #212121;
    }

    @media only screen and (max-width: 600px) {
        li.nav-item a {
            padding: 5px 15px !important;
            color: #212121 !important;
            text-decoration: none;
        }

        .custom_button_yellow {
            margin-bottom: 10px;
        }

        .custom_button_yellow_top {
            margin-top: 10px;
        }
    }
</style>

<header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid d-flex justify-content-between">
            <a href="{{ url('/')}}" class="navbar-brand" href="/">
                <img src="{{ url('/logo.png')}}" width="90px" alt="ISMAX">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="menu-list navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="/"><i class="fas fa-home menu-icon"></i>&nbsp;Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="/about-us">
                            <i class="fas fa-question-circle menu-icon"></i>
                            &nbsp;About Us
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle p-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fas fa-tasks menu-icon"></i>
                            &nbsp;Services
                        </a>
                        <ul class="dropdown-menu shadow" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="/security-guards">Security Guards</a></li>
                            <li><a class="dropdown-item" href="/consultancy">Security Consultancy</a></li>
                            <li><a class="dropdown-item" href="/alarm-systems">Alarm & Fire System</a></li>
                            <li><a class="dropdown-item" href="/risk-management">Risk Management</a></li>
                            <li><a class="dropdown-item" href="/event-security">Event Security and V.I.P Protection</a></li>
                            <li><a class="dropdown-item" href="/cctv-systems">CCTV and Live Monitoring</a></li>
                            <li><a class="dropdown-item" href="/other-services">Other Services</a></li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="/clients">
                            <i class="fas fa-smile menu-icon"></i>
                            &nbsp;Our Clients
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/careers"><i class="fas fa-graduation-cap menu-icon"></i>&nbsp;Careers</a>
                    </li>

                    <li class="nav-item">
                        <a href="/contact-us"><i class="fas fa-phone menu-icon"></i>&nbsp;Contact Us</a>
                    </li>
                    @if(Auth::user())
                    <li class="nav-item">
                        <a href="/jobs">Jobs</a>
                    </li>
                    <li class="nav-item">
                        <a href="/logout"><i class="fas fa-user menu-icon"></i>&nbsp;Logout</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

</header>
<!-- end header -->