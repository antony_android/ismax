<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Ismax Security Limited - Customized Security Solutions</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

  <!-- css -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <link href="/css/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="/css/flexslider.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <link href="/css/jquery.datetimepicker.css" rel="stylesheet" />
  <link href="/css/style.css" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

  <script src="/js/jquery-3.2.1.min.js"></script>
  <script src="/js/jquery.datetimepicker.js"></script>

</head>

<body>
  <div id="wrapper" class="home-page">
    @include('layouts/header')

    @yield('content')

    @include('layouts/footer')