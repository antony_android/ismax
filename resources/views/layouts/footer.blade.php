<footer style="font-weight:normal; font-size:14px;">
	<div class="container">
		<div class="justify-content-center">
			<div class="row">
				<div class="col-md-5">
					<h5 class="widgetheading"><strong>Physical Location</strong></h5>
					<div class="mapouter">
						<div class="gmap_canvas">
							<iframe width="250" height="150" id="gmap_canvas" src="https://maps.google.com/maps?q=Ole%20Shapara%20Road,%20South%20C&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
							<style>
								.mapouter {
									position: relative;
									text-align: right;
									height: 150px !important;
									width: 250px;
								}
							</style>
							<style>
								.gmap_canvas {
									overflow: hidden;
									background: none !important;
									height: 150px;
									width: 250px;
									border-radius: 10px;
								}
							</style>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<h6 class="footer-heading" style="text-align: left;"><strong>Quick Links</strong></h6>
					<ul style="list-style-type: none; padding-left: 0px;">
						<li class="footer-links"><a href="/"><i class="fas fa-home"></i> &nbsp; Home</a></li>
						<li class="footer-links"><a href="/about-us"><i class="fas fa-question-circle"></i> &nbsp; About Us</a></li>
						<li class="footer-links"><a href="/careers"> <i class="fas fa-graduation-cap"></i> &nbsp; Careers</a></li>
						<li class="footer-links"><a href="/contact-us"><i class="fas fa-phone"></i> &nbsp; Contact Us</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h6 class="footer-heading" style="text-align: left;"><strong>Contact Us</strong></h6>
					<ul class="footer-icons" style="list-style-type: none; padding-left: 0px;">
						<li class="footer-links"><i class="fa fa-phone fa-sm"></i>&nbsp; +254 (0) 786 600014, 733 155512</li>
						<li class="footer-links"><i class="fa fa-envelope fa-sm"></i>&nbsp;info@ismaxsecurity.com</li>
						<li class="footer-links"><i class="fa fa-globe fa-sm"></i>&nbsp;www.ismaxsecurity.com</li>
					</ul>
				</div>
			</div>
			<div class="social-media-wrapper" style="display: flex; justify-content: space-between;">

				<div class="col-md-12">
					<hr style="color:#CCC">
					<p class="footer_p text-center">
						Copyright © <?php echo date('Y') ?> ISMAX SECURITY. All Rights Reserved.
					</p>
				</div>

				</h6>
			</div>
		</div>
	</div>
</footer>
</div>
<!-- <a href="#" class="scrollup waves-effect waves-dark"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="js/jquery.js"></script> -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

<script src="/js/ckeditor.js"></script>

<script>
	toastr.options.closeButton = true;
	toastr.options.showMethod = 'slideDown';
	toastr.options.hideMethod = 'slideUp';
	toastr.options.closeMethod = 'slideUp';
	toastr.options.preventDuplicates = true;

	@if(Session::has('error'))
	toastr.error("{{ Session::get('error') }}", '', {
		timeOut: 7000
	});
	@endif;
	@if(Session::has('success'))
	toastr.success("{{ Session::get('success') }}", '', {
		timeOut: 4000,
		iconClass: 'toast-success',
	});
	@endif;
</script>

</body>

</html>