	@extends('layouts.app')

	<!-- Carousel section -->

	@section('content')

	<script>
		$(document).ready(function() {

			jQuery('#doe').datetimepicker();
			jQuery('#last_doe').datetimepicker();

			$('#last_doe').hide();
			$('#reason').hide();

			$('.in_service').change(function() {

				let selected = $("input[name=still_in_service]:checked").val();
				if (selected == "No") {

					$('#last_doe').show();
					$('#reason').show();
				} else {
					$('#last_doe').hide();
					$('#reason').hide();
				}
			});

		});

		function submitRequest() {

			let selected = $("input[name=still_in_service]:checked").val();

			if (selected == "No") {
				let last_doe = $('#last_doe').val();
				let reason = $('#reason').val();

				if (!last_doe || !reason) {

					alert("Please provide your last date of employment as well as the reason for leaving to continue");
					return;
				}
			}
			document.getElementById('request-certificate-form').submit();

		}
	</script>

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 ">
					<h1 class="pageTitle">Find a Job / Apply for ISMAX Certificate</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			@if(count($jobs) > 0)
			@foreach($jobs as $job)
			<div class="row" style="margin: 10px 0px;">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-1">
										<img src="/img/career.png" class="img-fluid" width="100px" alt="Career" />
									</div>
									<div class="col-sm-11">
										@if(strlen($job->position) > 0)
										<p>
											<span class="job-header">Position:&nbsp;</span>{!!$job->position !!}
										</p>
										@endif
										@if(strlen($job->job_type) > 0)
										<p>
											<span class="job-header">Job Type:&nbsp;</span>{!!$job->job_type !!}
										</p>
										@endif
										@if(strlen($job->location) > 0)
										<p>
											<span class="job-header">Location:&nbsp;</span>{!!$job->location !!}
										</p>
										@endif

										@if(strlen($job->minimum_qualification) > 0)
										<p>
											<span class="job-header">Minimum Qualification:&nbsp;</span>{!!$job->minimum_qualification !!}
										</p>
										@endif

										@if(strlen($job->experience_level) > 0)
										<p>
											<span class="job-header">Level of Experience:&nbsp;</span>{!!$job->experience_level !!}
										</p>
										@endif

										@if(strlen($job->experience_years) > 0)
										<p>
											<span class="job-header">Years of Experience:&nbsp;</span>{!!$job->experience_years !!}
										</p>
										@endif

										@if(strlen($job->job_summary) > 0)
										<p class="job-header">Job Summary</p>
										{!!$job->job_summary !!}
										@endif

										@if(strlen($job->job_description) > 0)
										<p class="job-header">Job Description</p>
										{!!$job->job_description !!}
										@endif

										@if(strlen($job->responsibilities) > 0)
										<p class="job-header">Responsibilities</p>
										{!!$job->responsibilities !!}
										@endif

										@if(strlen($job->required_experience) > 0)
										<p class="job-header">Required Experience</p>
										{!!$job->required_experience !!}
										@endif

										@if(strlen($job->application_instructions) > 0)
										<p class="job-header">How to Apply</p>
										{!!$job->application_instructions !!}
										@endif

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@else
			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-1">
										<img src="/img/career.png" class="img-fluid" width="100px" alt="Career" />
									</div>
									<div class="col-sm-11">
										<p>As a company, we value our human resources and we offer them an opportunity to grow both in personal development and in their career path. We are constantly looking to engage new talent and develop people to become the best version of themselves.</p>
										<p>We currently have no job openings. Please keep checking with us.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif

			<div class="row mt-15">
				<div class="col-sm-12 sub-title">
					Apply for Certificate
				</div>
			</div>
			<div class="row mt-15">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 p-content">
									<p>Do you need an Ismax certificate of service? Provide your details below.</p>
									<form id="request-certificate-form" method="POST" action="/certificate">
										@csrf
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name" placeholder="Full Name">
													@error('full_name')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="text" class="form-control @error('id_no') is-invalid @enderror" id="id_no" name="id_no" placeholder="Your National ID / Passport Number">
													@error('id_no')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="Your Phone Number">
													@error('phone')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="text" class="form-control @error('doe') is-invalid @enderror" id="doe" name="doe" placeholder="Your Date of Employment">
													@error('doe')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">

												<div class="form-group mb-3">
													<label for="">Are you still in our service?</label><br />
													<input class="in_service" type="radio" id="yes" name="still_in_service" value="Yes">
													<label for="yes">Yes</label>
													<input class="in_service" type="radio" id="no" name="still_in_service" value="No">
													<label for="no">No</label><br>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="text" class="form-control" id="last_doe" name="last_doe" placeholder="Your Last Date of Employment">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group mb-3">

													<input type="reason" class="form-control @error('reason') is-invalid @enderror" id="reason" name="reason" placeholder="Reason for Leaving">
													@error('reason')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group mb-3">

													<textarea class="form-control @error('interest') is-invalid @enderror" id="comments" name="comments" rows="3" placeholder="Any Further Comments"></textarea>
													@error('comments')
													<div class="invalid-feedback">{{ $message }}</div>
													@enderror
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success btn-sm form-control py-2 quote-butt" onclick="event.preventDefault(); submitRequest();">Request</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>

	@endsection