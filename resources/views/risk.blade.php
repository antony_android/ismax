	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="pageTitle">RISK MANAGEMENT</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-12">
										<img src="/img/risk-management.png" class="services-img" alt="Risk Management" />
										<div>
											<p>Statistics suggest that more than 80% of corporate fraud is committed by existing employees. Most cases are uncovered by accident, many when it’s too late. That’s why we place as much emphasis on prevention as we do an recovery. 8efare trouble strikes, we will audit and examine, ring-fence and upgrade, then re-examine – systems, people, processes, supply chains and more. We’II provide you with specific indicators you can monitor ta spat trouble. If what you suspect has already happened, we’ll investigate – covertly and discreetly – until we identify the source of the problem.</p>
											<p class="services-submenu">Vetting & Screening</p>
											<p>Security personnel need to be screened, this should apply in other professional walks of life. We can carry out independent vetting checks to various levels. As well as public sector employees, our Security Consultants are involved with the private sector, making tenant enquiries on behalf af landlords before tenancies begin, and helping them deal with nuisance tenants they may have inherited.</p>
											<p>As well as providing static security officers, we also move with you. Whether transporting freight by road and rail, our security solutions can provide the support and infrastructure you need to trace track and trail valuable goods. Our security consultants can analyze your plans,carrying out a comprehensive safety and security audit. We will put in place plant and personnel to help you monitor your assets every step of the way using sophisticated electronics. And we can report back to you moment by moment, saving you money in the process.</p>
											<p class="services-submenu">Warehouse Security</p>
											<img src="/img/warehouse.png" class="services-img" alt="Guards" />
											<p>Agile and attentive, our specially trained security Officers don’ t flinch when it comes to working in hostile environments. Quick to respond many come form the ranks of the Armed Forces-they will work in tande with CCTV surveillance cameras to scour every corner of your warehouses and distribution centers.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>

	@endsection