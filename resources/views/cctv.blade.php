	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12">
	                <h1 class="pageTitle">CCTV & LIVE MONITORING</h1>
	            </div>
	            <div class="overflow-sec primary-color">
	            </div>
	        </div>
	    </div>
	</section>

	<section id="about-content">
	    <div class="container content">
	        <!-- Service Blocks -->

	        <div class="row">
	            <div class="col-sm-12">
	                <div class="card about-wrap">
	                    <div class="card-body">
	                        <div class="page-info">
	                            <div class="row">
	                                <div class="col-sm-12">
	                                    <img src="/img/cctv-1.jpg" class="services-img" alt="Guards" />
	                                    <div>
	                                        <p>Ismax Security remote video surveillance watches the outside of your property. When monitors see suspicious activity they can play speakers and call police, often before criminals break in to buildings and cause damage, theft, and loss to outdoor & indoor assets.</p>
	                                        <p>Typical security systems are reactive. They’re designed for interior spaces. So they wait for a trigger to sound an alarm and alert the police AFTER criminals have already been on your property and broken into buildings. This often results in false calls, delayed police response time, and a greater opportunity for criminals to get away.</p>
	                                        <p>Tired of doing your own video searches? Ismax Security has a department that reviews all relevant cameras to provide a concise summary of what happened on camera with related police report data if available. Law enforcement officials love the work we do.</p>
	                                        <p>Ismax Security Monitoring takes a proactive approach with live monitoring and surveillance. Our highly trained staff monitors the outside and inside of your property to help prevent crimes before they happen. We don’t wait for alarms. We watch for and evaluate suspicious activity in real time. Our trained operators activate speakers at unwanted trespassers and they typically go running off your property since they had no idea someone was watching them! We call police with live reports on criminals often BEFORE they break in or do damage.</p>
	                                        <p>Ismax Security ’s remote video monitoring caters to clients looking to have incident footage with real-time intervention. From pan/tilt/zoom to thermal, Ismax Security ’s wide range of state-of-the-art cameras can cater to any security needs. Our video monitoring service additionally stores these recordings in both our Command Center and within the individual units, meaning that upon request, the Command Center is able to complete a full data review, pinpointing and delivering the incident for legal or internal purposes. Video monitoring offers you piece of mind.</p>
	                                        <p>Benefits of video monitoring include:</p>
	                                        <div class="row">
	                                            <div class="col-sm-5">

	                                                <ul style="list-style: none;line-height: 2;">
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Access Control</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Provide entry assistance with video confirmation</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Verify identity and authorization</li>
	                                                </ul>
	                                            </div>
	                                            <div class="col-sm-5">

	                                                <ul style="list-style: none;line-height: 2;">
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Provide real-time intervention</li>
	                                                    <li><i class="fas fa-check primary-color"></i>&nbsp;Provide snap-shots and video clips</li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>


	    </div>
	</section>

	@endsection