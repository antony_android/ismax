	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="pageTitle">SECURITY GUARDS</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-12">
										<img src="/img/guards.jpeg" class="services-img" alt="Guards" />
										<div>
											<p>Our Security Officers match the profile you’re looking for. A friendly smiling face to welcome visitors to your premises, or a more assertive no-nonsense approach to protect plant and machinery in the dead of night. Screened, vetted, qualified and experienced, there’s lots going on behind the uniform.</p>
											<p>Our Security Officers can anticipate a problem long before visitors,guests or shoppers suspect a thing. They can diffuse tensions using tact and diplomacy. They respond to difficult situations calmly and quickly.</p>
											<p>Our Security Officers are closely supervised and ably supported by their Team Leaders and Managers. Together, they have the kind of local knowledge and an-site training that will keep them a step ahead, whatever the threat. They also have the back-up of lsmax’s Security Control Room. Around the clack. This state-of-the-art nerve centre is at the forefront of CCTV technology, supporting Static Officers and Mobile Patrols in the field, keeping check an Lane Workers in isolated and often demanding workplaces indoors and out. Ready for the unexpected, they’re also lust a phone call away from our mast senior managers, ready to handle any crisis.</p>
											<p class="services-submenu">Front of House Security</p>
										</div>
									</div>
									<div class="col-sm-12">
										<img src="/img/front_desk.png" class="others-img-right" alt="Front Desk" style="margin-top: 0px;" />
										<div>
											<p>Meeting and greeting customers, or keeping a law-profile (in uniform or plain clothes), we’ re on watch. Our Front of House Security can blend seamlessly with your staff or stand out,be it for reassurance or as a deterrent.</p>
											<p>Whatever the point of entry, our officers are trained to search people, their bags, their rucksacks and hatch backs. Ismax Security can provide welcoming and attentive concierge services, too, for hotel and residential situations, at times to suit you. At Ismax Security, Front of House takes many forms, but the primary objective is the same.</p>
											As well as taking care of security issues, where
											circumstances permit and the client requests it, our
											lsmax Security officers will:
											<div class="row">
												<div class="col-sm-6">
													<ul style="list-style: none;line-height: 2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Keep tabs on the visitors' log</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Help visitors with directions</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Provide support to reception staff</li>
													</ul>
												</div>
												<div class="col-sm-6">
													<ul style="list-style: none;line-height:2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Escort the visitor to the concerned staff</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Handle first aid and other emergencies</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		</div>
	</section>

	@endsection