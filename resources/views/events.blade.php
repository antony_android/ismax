	@extends('layouts.app')

	@section('content')

	<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="pageTitle">EVENT SECURITY & V.I.P PROTECTION</h1>
				</div>
				<div class="overflow-sec primary-color">
				</div>
			</div>
		</div>
	</section>

	<section id="about-content">
		<div class="container content">
			<!-- Service Blocks -->

			<div class="row">
				<div class="col-sm-12">
					<div class="card about-wrap">
						<div class="card-body">
							<div class="page-info">
								<div class="row">
									<div class="col-sm-12">
										<img src="/img/event-security.png" class="services-img" alt="Guards" />
										<div>
											<p>Ismax Security, provides an Event Security Management Service that is highly professional and supported by cutting edge technology and by management experience.</p>
											<p>All our clients see our intelligent approach as a unique selling point. An easy option is us taking the hassle out of the security and being a one stop shop for your Event Security.</p>
											<p>Our Event Security Services suit the following events:</p>
											<div class="row">
												<div class="col-sm-4">

													<ul style="list-style: none;line-height: 2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Product Launches</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Art Exhibitions</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Fashion Shows</li>
													</ul>
												</div>
												<div class="col-sm-5">

													<ul style="list-style: none;line-height: 2;">
														<li><i class="fas fa-check primary-color"></i>&nbsp;Film Premieres</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Celebrity Appearances</li>
														<li><i class="fas fa-check primary-color"></i>&nbsp;Company AGM’s</li>
													</ul>
												</div>
											</div>
											<p>Each event is different and requires a unique approach. Our Management Team is experienced in Risk Assessments, problem solving and working within a security budget.</p>
											<p>Some locations can sometimes be a challenge but with our experience, we can offer our clients the best possible Event Security Service in any city, or remote location.</p>
											<p>No matter the need or event, we are able to supply the right personnel. From two operatives, to contracts involving 200+ operatives with Operational Management and Logistical Support.</p>
											<p class="services-submenu">VIP Protection</p>
											<p>We offer VIP Protection and Event Security. Because of the increased threat of worldwide and domestic terrorism and longstanding traditional threats, the need for a strategic and organized executive protection plan has never been greater. These plans are customized for industry leaders, corporate executives, entertainment celebrities, and prominent political leaders.</p>
											<p>If you need VIP Protection or Armed Guards, Ismax Security can develop a security and safety plan for an organization or individual to mitigate silent but constant threats such as kidnapping, child abduction, assault, intimidation, stalking, public harassment and alarm. Just as critical, but routinely ignored, is the close personal protection we provide to the family members of at-risk individuals.</p>
											<p>When it comes to executive protection and personal safety, our contacts and resources. We provide a highly trained organization of specialists that work to minimize our clients’ risks with a professional and highly sophisticated detachment of seasoned security professionals.</p>
											<p>Our agents work with local law enforcement agencies to provide an assessment and plan that includes: secure air and ground transportation, advanced security measures, counter-surveillance measures, and medical assistance. The key to our success is our ability to respect our clients’ desire to have a normal and unobstructed lifestyle while having peace of mind security.</p>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</section>

	@endsection