@extends('layouts.app')
@section('content')

<style>
    .ck-content {
        width: 100%;
        height: 150px;
    }
</style>

<script>
    function deleteJob(jobid) {

        if (confirm('Are you sure you want to delete this job record')) {

            document.getElementById('delete-job-form-' + jobid).submit();
        } else {
            return;
        }
    }

    $(document).ready(function() {

        $('#expiry_date').datetimepicker();

        var allEditors = document.querySelectorAll('.preview_txt');
        for (let editor of allEditors) {
            ClassicEditor.create(editor);
        }

        $(document).on("click", ".deljob", function() {
            var passedID = $(this).attr('rel');
            $("#del-job").val(passedID);
        });

    });
</script>
<div class="row">
    <div class="col-sm-12 add-container">
        <form action="/job/save" method="post" class="add-form">
            <div class="container">
                <div class="row">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Please fix the following errors
                    </div>
                    @endif
                    @if($selectedJob != null)
                    <input type="hidden" id="selected_job" name="selected_job" value="{{$selectedJob->id}}" />
                    @endif
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="position">Position</label>
                            <input type="text" class="form-control @error('position') is-invalid @enderror" id="position" name="position" placeholder="Job Position" value="@if($selectedJob != null){{$selectedJob->position}}@else{{ old('position') }}@endif">
                            @error('position')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="job_type">Job Type</label>

                            <select class="form-control" name="job_type" id="job_type">
                                <option value="">Select Job Type</option>
                                <option @if($selectedJob !=null) @if($selectedJob->job_type == "Permanent") selected @endif @endif value="Permanent">Permanent</option>
                                <option @if($selectedJob !=null) @if($selectedJob->job_type == "Casual") selected @endif @endif value="Casual">Casual</option>
                                <option @if($selectedJob !=null) @if($selectedJob->job_type == "Contract") selected @endif @endif value="Contract">Contract</option>
                            </select>
                            @error('job_type')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="location">Location</label>
                            <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" name="location" placeholder="Job Location" value="@if($selectedJob != null){{$selectedJob->location}}@else{{ old('location') }}@endif">
                            @error('location')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="minimum_qualification">Minimum Qualification</label>

                            <select class="form-control" name="minimum_qualification" id="minimum_qualification">
                                <option value="">Select Minimum Qualification</option>
                                <option @if($selectedJob !=null) @if($selectedJob->minimum_qualification == "High School") selected @endif @endif value="High School">High School</option>
                                <option @if($selectedJob !=null) @if($selectedJob->minimum_qualification == "College Certificate") selected @endif @endif value="College Certificate">College Certificate</option>
                                <option @if($selectedJob !=null) @if($selectedJob->minimum_qualification == "College Diploma") selected @endif @endif value="College Diploma">College Diploma</option>
                                <option @if($selectedJob !=null) @if($selectedJob->minimum_qualification == "Bachelor") selected @endif @endif value="Bachelor">Bachelor</option>
                                <option @if($selectedJob !=null) @if($selectedJob->minimum_qualification == "Masters") selected @endif @endif value="Masters">Masters</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="experience_level">Level of Experience</label>

                            <select class="form-control" name="experience_level" id="experience_level">
                                <option value="">Select Required Level of Experience</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_level == "Graduate Trainee") selected @endif @endif value="Graduate Trainee">Graduate Trainee</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_level == "Technical Experience") selected @endif @endif value="Technical Experience">Technical Experience</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_level == "Management Experience") selected @endif @endif value="Management Experience">Management Experience</option>
                            </select>
                            @error('experience_level')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="experience_years">Years of Experience</label>

                            <select class="form-control" name="experience_years" id="experience_years">
                                <option value="">Required Years of Experience</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "None") selected @endif @endif value="None">None</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "1") selected @endif @endif value="1">1</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "2") selected @endif @endif value="2">2</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "3") selected @endif @endif value="3">3</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "4") selected @endif @endif value="4">4</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "5+") selected @endif @endif value="5+">5+</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "10+") selected @endif @endif value="10+">10+</option>
                                <option @if($selectedJob !=null) @if($selectedJob->experience_years == "15+") selected @endif @endif value="15+">15+</option>
                            </select>
                            @error('experience_years')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="expiry_date">Expiration Date</label>
                            <input type="text" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiration Date" value="@if($selectedJob != null){{$selectedJob->expiry_date}}@else{{ old('expiry_date') }}@endif">
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-12">
                        <textarea class="form-control preview_txt" id="job_summary" name="job_summary" placeholder="Job Summary">@if($selectedJob != null){!! $selectedJob->job_summary !!}@endif</textarea>
                    </div>
                </div>

                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-12">
                        <textarea class="form-control preview_txt" id="job_description" name="job_description" placeholder="Job Description">@if($selectedJob != null){!! $selectedJob->job_description !!}@endif</textarea>
                    </div>
                </div>

                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-12">
                        <textarea class="form-control preview_txt" id="responsibilities" name="responsibilities" placeholder="Responsibilities">@if($selectedJob != null){!! $selectedJob->responsibilities !!}@endif</textarea>
                    </div>
                </div>

                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-12">
                        <textarea class="form-control preview_txt" id="required_experience" name="required_experience" placeholder="Required Experience">@if($selectedJob != null){!! $selectedJob->required_experience !!}@endif</textarea>
                    </div>
                </div>

                <div class="row" style="margin-top: 15px;">
                    <div class="col-sm-12">
                        <textarea class="form-control preview_txt" id="application_instructions" name="application_instructions" placeholder="Application Instructions">@if($selectedJob != null){!! $selectedJob->application_instructions !!}@endif</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary bt-margin" style="margin-top: 10px;">Save</button>
                        @if($selectedJob != null)
                        <a class="btn cancel-bt bt-margin" href="/jobs">
                            Cancel
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container cnt-bd">
    <div class="row search-mg">
        <div class="col-md-12 search-col">
            <button class="btn float-right search-bt" onclick="searchJobs()">Search</button>
            <input type="text" class="float-right search-txt" id="search-job" name="search-job" placeholder="Search" />
        </div>
    </div>
    <div class="row t-header">
        <div class="col-sm-2">Position</div>
        <div class="col-sm-2">Job Type</div>
        <div class="col-sm-2">Job Location</div>
        <div class="col-sm-2">Qualification</div>
        <div class="col-sm-2">Experience</div>
        <div class="col-sm-2"></div>
    </div>
    <div id="jobs-data">
        @foreach ($jobs as $job)
        <div class="row t-content">
            <div class="col-sm-2">
                {{ $job->position }}
            </div>
            <div class="col-sm-2">
                {{ $job->job_type }}
            </div>
            <div class="col-sm-2">
                {{ $job->location }}
            </div>
            <div class="col-sm-2">
                {{ $job->minimum_qualification }}
            </div>
            <div class="col-sm-2">
                {{ $job->experience_level }}
            </div>
            <div class="col-sm-2 edit-butts">
                <a href="/job/edit/{{$job->id}}">
                    <img src="/img/edit.png" width="15px" alt="Edit" />
                </a>

                <form id="delete-job-form-{{$job->id}}" action="/job/del" method="post">
                    @csrf
                    <input type="hidden" id="del-job-{{$job->id}}" name="deletejob" value="{{$job->id}}" />
                    <button class="deljob" type="submit" onclick="event.preventDefault(); deleteJob({{$job->id}});">
                        <img src="/img/delete.png" width="15px" alt="Delete" />
                    </button>
                </form>

            </div>
        </div>
        @endforeach
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="jobsModal" tabindex="-1" role="dialog" aria-labelledby="jobsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="jobsModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you would like to delete the selected job?
            </div>
            <div class="modal-footer">
                <div class="row" style="width:100%">
                    <div class="col-8">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                    <div class="col-4">
                        <form action="/job/del" method="post">
                            @csrf
                            <input type="hidden" id="del-job" name="deletejob" />
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection