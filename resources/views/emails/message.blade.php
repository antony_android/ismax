<html>

<head></head>

<body>
    <div style="padding: 30px;">
        <div style="width:100%;height:70px;background:#00A0DC;color:#FFF;font-size:15px;text-align:center;padding:10px 0px;">From Request for Quote Form</div>
        <div id="body" style="height: 400px;padding:20px;border:1px solid #ddd;">
            Dear Ismax,

            <p>There is an inquiry from the Website at the Send Message Form at the Contact Us Page with the following details:</p>

            <p>
                <strong>First Name: </strong> {{ $firstName }}
            </p>
            <p>
                <strong>Last Name: </strong> {{ $lastName }}
            </p>
            <p>
                <strong>Business Name: </strong> {{ $businessName }}
            </p>
            <p>
                <strong>Phone Number: </strong> {{ $phone }}
            </p>
            <p>
                <strong>Email Address: </strong> {{ $emailAddress }}
            </p>
            <p>
                <strong>Interest: </strong> {{ $interest }}
            </p>
            <p>
                <strong>Comments: </strong> {{ $comments }}
            </p>
        </div>
        <div id="footer" style="width:100%;height:50px;background:#00A0DC;color:#FFF;font-size:13px;text-align:center;padding:10px;">
            Copyright &copy; Ismax Security Limited
        </div>
    </div>
</body>

</html>