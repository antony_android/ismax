<html>

<head></head>

<body>
    <div style="padding: 30px;">
        <div style="width:100%;height:70px;background:#00A0DC;color:#FFF;font-size:15px;text-align:center;padding:10px 0px;">From Request for Quote Form</div>
        <div id="body" style="height: 400px;padding:20px;border:1px solid #ddd;">
            Dear Ismax,

            <p>There is a request for ISMAX certificate from the Website with the following details:</p>

            <p>
                <strong>Full Name: </strong> {{ $fullName }}
            </p>
            <p>
                <strong>ID / Passport Number: </strong> {{ $idNo }}
            </p>
            <p>
                <strong>Phone Number: </strong> {{ $phone }}
            </p>
            <p>
                <strong>Date of Employment: </strong> {{ $doe }}
            </p>
            @if(!is_null($lastDoe) && strlen($lastDoe) > 0)
            <p>
                <strong>Last Date of Employment: </strong> {{ $lastDoe }}
            </p>
            @endif
            @if(!is_null($reason) && strlen($reason) > 0)
            <p>
                <strong>Reason for Leaving: </strong> {{ $reason }}
            </p>
            @endif
            <p>
                <strong>Comments: </strong> {{ $comments }}
            </p>
        </div>
        <div id="footer" style="width:100%;height:50px;background:#00A0DC;color:#FFF;font-size:13px;text-align:center;padding:10px;">
            Copyright &copy; Ismax Security Limited
        </div>
    </div>
</body>

</html>