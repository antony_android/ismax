<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/jobs', 'JobsController@index');
Route::post('/job/save', 'JobsController@save')->name('save');
Route::post('/jobs/search', 'JobsController@search')->name('search');
Route::get('/job/edit/{id}', 'JobsController@edit')->name('edit');
Route::post('/job/del', 'JobsController@del')->name('del');

// Route::group(['prefix' => 'admin'], function () {
//     Voyager::routes();
// });

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::get('contact-us', 'HomeController@contact_us');
Route::get('about-us', 'HomeController@about_us');
Route::get('careers', 'HomeController@career');

Route::get('security-guards', 'HomeController@guards');
Route::get('consultancy', 'HomeController@consultancy');
Route::get('alarm-systems', 'HomeController@alarms');
Route::get('risk-management', 'HomeController@risk');
Route::get('event-security', 'HomeController@events');
Route::get('cctv-systems', 'HomeController@cctv');
Route::get('other-services', 'HomeController@otherServices');
Route::get('clients', 'HomeController@clients');
Route::post('quote', 'HomeController@quote');
Route::post('message', 'HomeController@message');
Route::post('certificate', 'HomeController@certificate');

// Route::get('{slug}', 'HomeController@page')->where('slug', '.+');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
