-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: psl
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int unsigned DEFAULT NULL,
  `order` int NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2021-06-17 04:51:23','2021-06-17 04:51:23'),(2,NULL,1,'Category 2','category-2','2021-06-17 04:51:23','2021-06-17 04:51:23'),(3,NULL,1,'Services','services','2021-06-17 05:04:59','2021-06-17 05:04:59');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_rows` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(22,4,'id','number','ID',1,0,0,0,0,0,NULL,1),(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(24,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(25,4,'name','text','Name',1,1,1,1,1,1,NULL,4),(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(27,4,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,6),(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(29,5,'id','number','ID',1,0,0,0,0,0,NULL,1),(30,5,'author_id','text','Author',1,0,1,1,0,1,NULL,2),(31,5,'category_id','text','Category',1,0,1,1,1,0,NULL,3),(32,5,'title','text','Title',1,1,1,1,1,1,NULL,4),(33,5,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,5),(34,5,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,6),(35,5,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(37,5,'meta_description','text_area','Meta Description',1,0,1,1,1,1,NULL,9),(38,5,'meta_keywords','text_area','Meta Keywords',1,0,1,1,1,1,NULL,10),(39,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(40,5,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,12),(41,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),(42,5,'seo_title','text','SEO Title',0,1,1,1,1,1,NULL,14),(43,5,'featured','checkbox','Featured',1,1,1,1,1,1,NULL,15),(44,6,'id','number','ID',1,0,0,0,0,0,NULL,1),(45,6,'author_id','text','Author',1,0,0,0,0,0,NULL,2),(46,6,'title','text','Title',1,1,1,1,1,1,NULL,3),(47,6,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,4),(48,6,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,5),(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(50,6,'meta_description','text','Meta Description',1,0,1,1,1,1,NULL,7),(51,6,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,NULL,8),(52,6,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(53,6,'created_at','timestamp','Created At',1,1,1,0,0,0,NULL,10),(54,6,'updated_at','timestamp','Updated At',1,0,0,0,0,0,NULL,11),(55,6,'image','image','Page Image',0,1,1,1,1,1,NULL,12);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2021-06-17 04:50:45','2021-06-17 04:50:45'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2021-06-17 04:50:45','2021-06-17 04:50:45'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController','',1,0,NULL,'2021-06-17 04:50:46','2021-06-17 04:50:46'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2021-06-17 04:51:20','2021-06-17 04:51:20'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,NULL,'2021-06-17 04:51:24','2021-06-17 04:51:24'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2021-06-17 04:51:29','2021-06-17 04:51:29');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2021-06-17 04:50:54','2021-06-17 04:50:54','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2021-06-17 04:50:54','2021-06-17 04:50:54','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2021-06-17 04:50:54','2021-06-17 04:50:54','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2021-06-17 04:50:55','2021-06-17 04:50:55',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2021-06-17 04:50:55','2021-06-17 04:50:55','voyager.settings.index',NULL),(11,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2021-06-17 04:51:22','2021-06-17 04:51:22','voyager.categories.index',NULL),(12,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2021-06-17 04:51:27','2021-06-17 04:51:27','voyager.posts.index',NULL),(13,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2021-06-17 04:51:31','2021-06-17 04:51:31','voyager.pages.index',NULL),(14,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2021-06-17 04:51:41','2021-06-17 04:51:41','voyager.hooks',NULL),(15,2,'Home','/','_self',NULL,'#000000',NULL,1,'2021-06-17 04:58:18','2021-06-29 10:04:25',NULL,''),(16,2,'About  us','about-us','_self',NULL,'#000000',NULL,2,'2021-06-29 09:52:28','2021-06-29 10:04:26',NULL,''),(17,2,'Services','services','_self',NULL,'#000000',NULL,3,'2021-06-29 10:03:13','2021-06-29 10:04:26',NULL,''),(18,2,'CCTV  & Live Monitoring','cctv-and-live-monitoring','_self',NULL,'#000000',17,1,'2021-06-29 10:04:19','2021-06-29 10:04:26',NULL,''),(19,2,'Event Security and VIP Protection','event-security-and-vip-protection','_self',NULL,'#000000',17,2,'2021-06-29 10:05:35','2021-06-29 10:05:40',NULL,''),(20,2,'Risk Management','risk-management','_self',NULL,'#000000',17,3,'2021-06-29 10:06:22','2021-06-29 10:06:26',NULL,''),(21,2,'Alarm and Fire System','alarm-and-fire-system','_self',NULL,'#000000',17,4,'2021-06-29 10:07:20','2021-06-29 10:07:41',NULL,''),(22,2,'Security Consultancy','security-consultancy','_self',NULL,'#000000',17,5,'2021-06-29 10:08:12','2021-06-29 10:08:21',NULL,''),(23,2,'Security guards','security-guards','_self',NULL,'#000000',17,6,'2021-06-29 10:09:02','2021-06-29 10:09:07',NULL,''),(24,2,'Why us','why-us','_self',NULL,'#000000',NULL,15,'2021-06-29 10:11:27','2021-06-29 10:11:27',NULL,''),(25,2,'Careers','careers','_self',NULL,'#000000',NULL,16,'2021-06-29 10:12:03','2021-06-29 10:12:03',NULL,''),(26,2,'Contact us','contact-us','_self',NULL,'#000000',NULL,17,'2021-06-29 10:12:33','2021-06-29 10:12:33',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2021-06-17 04:50:54','2021-06-17 04:50:54'),(2,'main','2021-06-17 04:55:58','2021-06-17 04:55:58');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1),(24,'2016_01_01_000000_create_pages_table',2),(25,'2016_01_01_000000_create_posts_table',2),(26,'2016_02_15_204651_create_categories_table',2),(27,'2017_04_11_000000_alter_post_nullable_fields_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2021-06-17 04:51:32','2021-06-17 04:51:32'),(2,1,'About  us','PSL is an integrated security firm that specializes in the planning and security risk mitigation, manned and mobile security, consulting and risk management.','<p><img src=\"storage/page/about.png\" alt=\"\" /></p>\r\n<p>PSL is an integrated security firm that specializes in the planning and security risk mitigation, manned and mobile security, consulting and risk management.</p>\r\n<p>We at PSL Security, recognize security as a discipline that requires a holistic view and approach to be successful in mitigating a client&rsquo;s risks and vulnerabilities. Security plays a strategic role in supporting a business process, and a progressively vital role in the relationship between an organization and its customers, partners, and employees.</p>\r\n<p>&nbsp;We at PSL Security, recognize security as a discipline that requires a holistic view and approach to be successful in mitigating a client&rsquo;s risks and vulnerabilities. Security plays a strategic role in supporting a business process, and a progressively vital role in the relationship between an organization and its customers, partners, and employees.</p>','pages/June2021/dqTd6n7ZJKShtf5AtoIN.png','about-us','About us,PSL','About us,PSL','ACTIVE','2021-06-17 05:13:05','2021-06-29 18:54:43'),(3,1,'Security Consultancy','We can help you create a robust security environment with services that include threat assessments, policy review and development, and master planning','<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>We can help you create a robust security environment with services that include threat assessments, policy review and development, and master planning.<br /><br />Security decisions you make today can determine your organization&rsquo;s security and resilience for years to come. Our comprehensive security consulting services enable you to feel more confident about the actions you take to protect your family office, employees, operations, facilities, and assets.<br /><br />Our security consultants have decades of experience advising private clients and corporations across industries that range from construction, manufacturing, and transportation to education, hospitality, and government. We can help you create a robust security environment with services that include current and emerging threat assessments, policy review and development, and master planning.</p>\r\n</div>\r\n</div>\r\n<h6 class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734831672\" style=\"font-size: 20px; color: #00b0f0; text-align: left;\">Integrated security framework for stronger risk mitigation</h6>\r\n<p>Based on time-tested best practices as well as real-world experience, our holistic approach reflects the interrelated nature of today&rsquo;s businesses. It&rsquo;s a classic case where the whole is greater than the sum of its parts &mdash; an integrated security framework provides for stronger, more cohesive protection to mitigate threats to your organization.<br /><br />For example, there are physical, operational. By looking at your security challenges from several vantage points, a Ismax specialist can help you better prevent, plan for, and respond to threats.<br /><br />Our security consulting services include:</p>\r\n<div class=\"vc_row wpb_row vc_inner vc_row-fluid\">\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Threat and vulnerability assessments</li>\r\n<li>Policy and procedure review and development</li>\r\n<li>Security audits</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Security training</li>\r\n<li>Security master planning</li>\r\n<li>Securing intellectual property</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',NULL,'security-consultancy','security consultancy','security consultancy','ACTIVE','2021-06-17 05:16:19','2021-06-17 05:16:19'),(4,1,'Security guards','Our Security Officers match the profile you’re looking for. A friendly smiling face to welcome visitors to your premises, or a more assertive no-nonsense approach to protect plant and machinery in the dead of night. Screened, vetted, qualified and experienced, there’s lots going on behind the uniform.','<h3>Total security guaranteed</h3>\r\n<p>Our Security Officers match the profile you&rsquo;re looking for. A friendly smiling face to welcome visitors to your premises, or a more assertive no-nonsense approach to protect plant and machinery in the dead of night. Screened, vetted, qualified and experienced, there&rsquo;s lots going on behind the uniform.</p>\r\n<p>Our Security Officers can anticipate a problem long before visitors,guests or shoppers suspect a thing. They can diffuse tensions using tact and diplomacy. They respond to difficult situations calmly and quickly.</p>\r\n<p>Our Security Officers are closely supervised and ably supported by their Team Leaders and Managers. Together, they have the kind of local knowledge and an-site training that will keep them a step ahead, whatever the threat. They also have the back-up of lsmax&rsquo;s Security Control Room. Around the clack. This state-of-the-art nerve centre is at the forefront of CCTV technology, supporting Static Officers and Mobile Patrols in the field, keeping check an Lane Workers in isolated and often demanding workplaces indoors and out. Ready for the unexpected, they&rsquo;re also lust a phone call away from our mast senior managers, ready to handle any crisis.</p>\r\n<h2 style=\"color: #00b0f0;\">Front of House Security</h2>\r\n<p>Our front of house officers are trained to provide great customer service with clear communication Skills. Trained and expected&nbsp;to be neat and smart all the time, we deal with all customers in a courteous and professional manner The officers have also gone through&nbsp;conflict management training, which equips them with the relevant qualities to keep your premises safe and secure.</p>\r\n<p>Whatever the point of entry, our officers are trained to search people, their bags, their rucksacks and hatch backs. PSL can provide welcoming and attentive concierge services, too, for hotel and residential situations, at times to suit you. At PSL, Front of House takes many forms, but the primary objective is the same.</p>',NULL,'security-guards','security guards,Front of house  security','security guards,Front of house  security','ACTIVE','2021-06-17 05:18:21','2021-06-17 06:01:32'),(5,1,'Alarm and  Fire System','Through our outstanding officers performance and the large fleet, we believe that we deliver the fastest response times in the industry. Although crime prevention is important.The tasks of monitoring a facility or application for potential fire threats or operating a releasing system are facilitated by a fire detection and alarm system.','<h2 class=\"vc_custom_heading vcr_float_right vc_custom_1531732047950\" style=\"font-size: 28px; color: #00b0f0; text-align: left;\">Alarm and Fire System</h2>\r\n<p class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734681502\" style=\"font-size: 18px; text-align: left;\">&nbsp;</p>\r\n<p class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734681502\" style=\"font-size: 18px; text-align: left;\"><strong>Fire alarm technology will keep your family safe, no matter what happens</strong></p>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>Through our outstanding officers performance and the large fleet, we believe that we deliver the fastest response times in the industry. Although crime prevention is important.The tasks of monitoring a facility or application for potential fire threats or operating a releasing system are facilitated by a fire detection and alarm system.<br /><br />Fire detection devices, typically smoke, heat or optical fire detectors, electronically relay fire events to a fire control panel which processes the fire detection signals from protected areas and immediately performs key operations, including sounding alarms, shutting down equipment, and releasing fire suppression systems. The control panel also facilitates input signals from manual stations, as well as audible and visual notification devices.<br /><br />These systems are supervised to ensure circuit integrity. In addition, the control panels are supplied with battery backup in the event of power loss.<br /><br />We offer emergency alarm response services for all properties. If your security system has been activated whilst you are away from your home or office, our trained Key Guards can provide fast response at any time, day or night. Our alarm response services include rapid response to:</p>\r\n<div class=\"vc_row wpb_row vc_inner vc_row-fluid\">\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Alarm system activations</li>\r\n<li>Reports of break-ins</li>\r\n<li>Attendance requests from emergency services</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<br /><br />When an alarm sounds on your property, your alarm company can communicate directly with our dispatch center and an officer will immediately be dispatched to inspect your property. When there is damage or a threat to your property, an officer will secure the area and notify the proper authorities as well as property management.<br /><br />Each time our officers respond, they will complete a detailed report, which will be sent to you for your records. Our services will help you avoid the extra cost in fees and fines from the city and counties for responding to false alarms.</div>\r\n</div>\r\n<h6 class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734831672\" style=\"font-size: 20px; color: #00b0f0; text-align: left;\">Patrol &amp; Alarm Response Services</h6>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>In select markets throughout the country, PSL Security provides security patrol services to enhance safety as well as to assist in the prevention, deterrence, and detection of trespassing, vandalism and criminal activity at your property. Our patrol officers are highly trained, uniformed personnel who patrol property in clearly marked vehicles. If we find evidence of or observe criminal activity, trespassing, vandalism, or simply anything out of the ordinary, our patrol officers immediately notify local law enforcement and client representatives.<br /><br />PSL Security&rsquo;s patrol services are customized to meet the unique needs of each client and can include randomly scheduled patrol service, regularly scheduled patrol service, response to emergency situations, temporary coverage, or scheduled services like monitoring equipment, providing escort services, or unlocking doors at specific times at your facility.<br /><br />Patrol officers are also available to respond to building alarms when notified by our dispatch center. After responding to the alarm, the patrol officers will notify local law enforcement and client representatives if we find evidence of criminal activity or vandalism at the property. Ismax Security&rsquo;s alarm response service provides quick notification of security issues related to the alarm at your property while reducing frustration and costs associated with false alarms.<br /><br />Through our patrol and alarm response services, the same industry leading security service provided to major corporations throughout the country is available and affordable for small businesses.</p>\r\n</div>\r\n</div>',NULL,'alarm-and-fire-system','Alarm  and  Fire  systems','alarm and  fire systems','ACTIVE','2021-06-17 05:21:23','2021-06-17 05:42:29'),(6,1,'Risk Management','Statistics suggest that more than 80% of corporate fraud is committed by existing employees.','<h2 class=\"vc_custom_heading vcr_float_right vc_custom_1531732047950\" style=\"font-size: 28px; color: #00b0f0; text-align: left;\">Risk Management</h2>\r\n<p class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734681502\" style=\"font-size: 18px; text-align: left;\">&nbsp;</p>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>Statistics suggest that more than 80% of corporate fraud is committed by existing employees. Most cases are uncovered by accident, many when it&rsquo;s too late. That&rsquo;s why we place as much emphasis on prevention as we do a recovery. 8efare trouble strikes, we will audit and examine, ring-fence and upgrade, then re-examine &ndash; systems, people, processes, supply chains and more. We&rsquo;II provide you with specific indicators you can monitor ta spat trouble. If what you suspect has already happened, we&rsquo;ll investigate &ndash; covertly and discreetly &ndash; until we identify the source of the problem.</p>\r\n</div>\r\n</div>\r\n<h6 class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734831672\" style=\"font-size: 20px; color: #00b0f0; text-align: left;\">Vetting &amp; Screening</h6>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>Just GS Officers in the security industry have to be screened, the same applies in other professional walks of life. We can carry out independent vetting checks to various levels. As well as public sector employees, our Security Consultants are involved with the private sector, making tenant enquiries on behalf af landlords before tenancies begin, and helping them deal with nuisance tenants they may have inherited.<br /><br />As well as providing static security officers, we also move with you. Whether transporting freight by road and rail, our security solutions can provide the support and infrastructure you need to trace track and trail valuable goods. Our security consultants can analyze your plans,carrying out a comprehensive safety and security audit. We will put in place plant and personnel to help you monitor your assets every step of the way using sophisticated electronics. And we can report back to you moment by moment, saving you money in the process.</p>\r\n</div>\r\n</div>\r\n<h6 class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734831672\" style=\"font-size: 20px; color: #00b0f0; text-align: left;\">Warehouse Security</h6>\r\n<div class=\"wpb_text_column wpb_content_element \">Agile and attentive, our specially trained security Officers don&rsquo; t flinch when it comes to working in hostile environments. Quick to respond many come form the ranks of the Armed Forces-they will work in tande with CCTV surveillance cameras to scour every corner of your warehouses and distribution centers.</div>',NULL,'risk-management','Risk management','Risk managent','ACTIVE','2021-06-17 05:24:08','2021-06-17 05:24:08'),(7,1,'Event Security and VIP Protection','Event Security and VIP Protection','<h2 class=\"vc_custom_heading vcr_float_right vc_custom_1531732047950\" style=\"font-size: 28px; color: #00b0f0; text-align: left;\">Event Security and VIP Protection</h2>\r\n<p class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734681502\" style=\"font-size: 18px; text-align: left;\">&nbsp;</p>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>PSL provides an Event Security Management Service that is highly professional and supported by cutting edge technology and by management experience.<br /><br />All our clients see our intelligent approach as a unique selling point. An easy option is us taking the hassle out of the security and being a one stop shop for your Event Security.<br /><br />Our Event Security Services suit the following events:</p>\r\n<div class=\"vc_row wpb_row vc_inner vc_row-fluid\">\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Product Launches</li>\r\n<li>Art Exhibitions</li>\r\n<li>Fashion Shows</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Film Premieres</li>\r\n<li>Celebrity Appearances</li>\r\n<li>Company AGM&rsquo;s</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<br /><br />Each event is different and requires a unique approach. Our Management Team is experienced in Risk Assessments, problem solving and working within a security budget.<br /><br />Some locations can sometimes be a challenge but with our experience, we can offer our clients the best possible Event Security Service in any city, or remote location.<br /><br />No matter the need or event, we are able to supply the right personnel. From two operatives, to contracts involving 200+ operatives with Operational Management and Logistical Support.</div>\r\n</div>\r\n<h6 class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734831672\" style=\"font-size: 20px; color: #00b0f0; text-align: left;\">VIP Protection</h6>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p>We offer VIP Protection and Event Security. Because of the increased threat of worldwide and domestic terrorism and longstanding traditional threats, the need for a strategic and organized executive protection plan has never been greater. These plans are customized for industry leaders, corporate executives, entertainment celebrities, and prominent political leaders.<br /><br />If you need VIP Protection or Armed Guards, PSL&nbsp; can develop a security and safety plan for an organization or individual to mitigate silent but constant threats such as kidnapping, child abduction, assault, intimidation, stalking, public harassment and alarm. Just as critical, but routinely ignored, is the close personal protection we provide to the family members of at-risk individuals.<br /><br />When it comes to executive protection and personal safety, our contacts and resources. We provide a highly trained organization of specialists that work to minimize our clients&rsquo; risks with a professional and highly sophisticated detachment of seasoned security professionals.<br /><br />Our agents work with local law enforcement agencies to provide an assessment and plan that includes: secure air and ground transportation, advanced security measures, counter-surveillance measures, and medical assistance. The key to our success is our ability to respect our clients&rsquo; desire to have a normal and unobstructed lifestyle while having peace of mind security.</p>\r\n</div>\r\n</div>',NULL,'event-security-and-vip-protection','Event  security','Event  security,VIP protection','ACTIVE','2021-06-17 05:29:38','2021-06-17 05:40:20'),(8,1,'CCTV & Live Monitoring','PSL Security remote video surveillance watches the outside of your property. When monitors see suspicious activity they can play speakers and call police, often before criminals break in to buildings and cause damage, theft, and loss to outdoor & indoor assets.','<div id=\"post-306\" class=\"post-306 page type-page status-publish hentry\">\r\n<div class=\"vc_row wpb_row vc_row-fluid\">\r\n<div class=\"wpb_column vc_column_container vc_col-sm-12\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<p class=\"vc_custom_heading vcr_float_right dt-skin-primary-color vc_custom_1531734681502\" style=\"font-size: 18px; text-align: left;\"><span style=\"font-size: 14px;\">PSL Security remote video surveillance watches the outside of your property. When monitors see suspicious activity they can play speakers and call police, often before criminals break in to buildings and cause damage, theft, and loss to outdoor &amp; indoor assets.</span></p>\r\n<div class=\"wpb_text_column wpb_content_element \">\r\n<div class=\"wpb_wrapper\">\r\n<p><br />Typical security systems are reactive. They&rsquo;re designed for interior spaces. So they wait for a trigger to sound an alarm and alert the police AFTER criminals have already been on your property and broken into buildings. This often results in false calls, delayed police response time, and a greater opportunity for criminals to get away.<br /><br />Tired of doing your own video searches? PSL has a department that reviews all relevant cameras to provide a concise summary of what happened on camera with related police report data if available. Law enforcement officials love the work we do.<br /><br />&nbsp;PSL Monitoring takes a proactive approach with live monitoring and surveillance. Our highly trained staff monitors the outside and inside of your property to help prevent crimes before they happen. We don&rsquo;t wait for alarms. We watch for and evaluate suspicious activity in real time. Our trained operators activate speakers at unwanted trespassers and they typically go running off your property since they had no idea someone was watching them! We call police with live reports on criminals often BEFORE they break in or do damage.<br /><br />&nbsp;PSL&rsquo;s remote video monitoring caters to clients looking to have incident footage with real-time intervention. From pan/tilt/zoom to thermal, PSL Security &rsquo;s wide range of state-of-the-art cameras can cater to any security needs. Our video monitoring service additionally stores these recordings in both our Command Center and within the individual units, meaning that upon request, the Command Center is able to complete a full data review, pinpointing and delivering the incident for legal or internal purposes. Video monitoring offers you piece of mind.<br /><br /><strong>Benefits of video monitoring include:</strong></p>\r\n<div class=\"vc_row wpb_row vc_inner vc_row-fluid\">\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">\r\n<ul class=\"dt-sc-fancy-list  listing check\">\r\n<li>Access Control</li>\r\n<li>Provide entry assistance with video confirmation</li>\r\n<li>Verify identity and authorization</li>\r\n<li>Provide real-time intervention</li>\r\n<li>Provide snap-shots and video clips</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"wpb_column vc_column_container vc_col-sm-4\">\r\n<div class=\"vc_column-inner \">\r\n<div class=\"wpb_wrapper\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>',NULL,'cctv-and-live-monitoring','CCTV & Live Monitoring','cctv,live monitoring','ACTIVE','2021-06-17 05:37:28','2021-06-29 18:57:47'),(9,1,'Careers','Careers','<p>Sorry&nbsp; we do not have openings at the&nbsp; moment.</p>',NULL,'careers','careers','careers','ACTIVE','2021-06-17 05:55:11','2021-06-17 05:55:11'),(10,1,'Why us','Why us','<p>PSL Security Limited provides security services With a proven record of accomplishment In Ethiopia We have a 24-hours supervision; the Supervisors check the posts and the Security officers randomly at any location</p>\r\n<p>All clients have access to our Management 24 hours a day,Our company is compliant with all Government statutory requirements.</p>\r\n<p>We guarantee the best services.</p>\r\n<p>Our prices are competitively fair.</p>',NULL,'why-us','why us','why  us','ACTIVE','2021-06-29 10:11:05','2021-06-29 10:11:05');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2021-06-17 04:50:56','2021-06-17 04:50:56'),(2,'browse_bread',NULL,'2021-06-17 04:50:57','2021-06-17 04:50:57'),(3,'browse_database',NULL,'2021-06-17 04:50:57','2021-06-17 04:50:57'),(4,'browse_media',NULL,'2021-06-17 04:50:57','2021-06-17 04:50:57'),(5,'browse_compass',NULL,'2021-06-17 04:50:57','2021-06-17 04:50:57'),(6,'browse_menus','menus','2021-06-17 04:50:57','2021-06-17 04:50:57'),(7,'read_menus','menus','2021-06-17 04:50:57','2021-06-17 04:50:57'),(8,'edit_menus','menus','2021-06-17 04:50:58','2021-06-17 04:50:58'),(9,'add_menus','menus','2021-06-17 04:50:58','2021-06-17 04:50:58'),(10,'delete_menus','menus','2021-06-17 04:50:58','2021-06-17 04:50:58'),(11,'browse_roles','roles','2021-06-17 04:50:58','2021-06-17 04:50:58'),(12,'read_roles','roles','2021-06-17 04:50:58','2021-06-17 04:50:58'),(13,'edit_roles','roles','2021-06-17 04:50:58','2021-06-17 04:50:58'),(14,'add_roles','roles','2021-06-17 04:50:59','2021-06-17 04:50:59'),(15,'delete_roles','roles','2021-06-17 04:50:59','2021-06-17 04:50:59'),(16,'browse_users','users','2021-06-17 04:50:59','2021-06-17 04:50:59'),(17,'read_users','users','2021-06-17 04:50:59','2021-06-17 04:50:59'),(18,'edit_users','users','2021-06-17 04:50:59','2021-06-17 04:50:59'),(19,'add_users','users','2021-06-17 04:51:00','2021-06-17 04:51:00'),(20,'delete_users','users','2021-06-17 04:51:00','2021-06-17 04:51:00'),(21,'browse_settings','settings','2021-06-17 04:51:00','2021-06-17 04:51:00'),(22,'read_settings','settings','2021-06-17 04:51:00','2021-06-17 04:51:00'),(23,'edit_settings','settings','2021-06-17 04:51:00','2021-06-17 04:51:00'),(24,'add_settings','settings','2021-06-17 04:51:00','2021-06-17 04:51:00'),(25,'delete_settings','settings','2021-06-17 04:51:00','2021-06-17 04:51:00'),(26,'browse_categories','categories','2021-06-17 04:51:22','2021-06-17 04:51:22'),(27,'read_categories','categories','2021-06-17 04:51:22','2021-06-17 04:51:22'),(28,'edit_categories','categories','2021-06-17 04:51:22','2021-06-17 04:51:22'),(29,'add_categories','categories','2021-06-17 04:51:23','2021-06-17 04:51:23'),(30,'delete_categories','categories','2021-06-17 04:51:23','2021-06-17 04:51:23'),(31,'browse_posts','posts','2021-06-17 04:51:27','2021-06-17 04:51:27'),(32,'read_posts','posts','2021-06-17 04:51:27','2021-06-17 04:51:27'),(33,'edit_posts','posts','2021-06-17 04:51:27','2021-06-17 04:51:27'),(34,'add_posts','posts','2021-06-17 04:51:27','2021-06-17 04:51:27'),(35,'delete_posts','posts','2021-06-17 04:51:28','2021-06-17 04:51:28'),(36,'browse_pages','pages','2021-06-17 04:51:31','2021-06-17 04:51:31'),(37,'read_pages','pages','2021-06-17 04:51:31','2021-06-17 04:51:31'),(38,'edit_pages','pages','2021-06-17 04:51:32','2021-06-17 04:51:32'),(39,'add_pages','pages','2021-06-17 04:51:32','2021-06-17 04:51:32'),(40,'delete_pages','pages','2021-06-17 04:51:32','2021-06-17 04:51:32'),(41,'browse_hooks',NULL,'2021-06-17 04:51:41','2021-06-17 04:51:41');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int NOT NULL,
  `category_id` int DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2021-06-17 04:51:28','2021-06-17 04:51:28'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2021-06-17 04:51:28','2021-06-17 04:51:28'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2021-06-17 04:51:28','2021-06-17 04:51:28'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2021-06-17 04:51:29','2021-06-17 04:51:29');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2021-06-17 04:50:56','2021-06-17 04:50:56'),(2,'user','Normal User','2021-06-17 04:50:56','2021-06-17 04:50:56');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2021-06-17 04:51:32','2021-06-17 04:51:32'),(2,'data_types','display_name_singular',6,'pt','Página','2021-06-17 04:51:32','2021-06-17 04:51:32'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2021-06-17 04:51:33','2021-06-17 04:51:33'),(4,'data_types','display_name_singular',4,'pt','Categoria','2021-06-17 04:51:33','2021-06-17 04:51:33'),(5,'data_types','display_name_singular',2,'pt','Menu','2021-06-17 04:51:33','2021-06-17 04:51:33'),(6,'data_types','display_name_singular',3,'pt','Função','2021-06-17 04:51:34','2021-06-17 04:51:34'),(7,'data_types','display_name_plural',5,'pt','Posts','2021-06-17 04:51:34','2021-06-17 04:51:34'),(8,'data_types','display_name_plural',6,'pt','Páginas','2021-06-17 04:51:34','2021-06-17 04:51:34'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2021-06-17 04:51:34','2021-06-17 04:51:34'),(10,'data_types','display_name_plural',4,'pt','Categorias','2021-06-17 04:51:35','2021-06-17 04:51:35'),(11,'data_types','display_name_plural',2,'pt','Menus','2021-06-17 04:51:35','2021-06-17 04:51:35'),(12,'data_types','display_name_plural',3,'pt','Funções','2021-06-17 04:51:35','2021-06-17 04:51:35'),(13,'categories','slug',1,'pt','categoria-1','2021-06-17 04:51:35','2021-06-17 04:51:35'),(14,'categories','name',1,'pt','Categoria 1','2021-06-17 04:51:35','2021-06-17 04:51:35'),(15,'categories','slug',2,'pt','categoria-2','2021-06-17 04:51:36','2021-06-17 04:51:36'),(16,'categories','name',2,'pt','Categoria 2','2021-06-17 04:51:36','2021-06-17 04:51:36'),(17,'pages','title',1,'pt','Olá Mundo','2021-06-17 04:51:36','2021-06-17 04:51:36'),(18,'pages','slug',1,'pt','ola-mundo','2021-06-17 04:51:36','2021-06-17 04:51:36'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2021-06-17 04:51:36','2021-06-17 04:51:36'),(20,'menu_items','title',1,'pt','Painel de Controle','2021-06-17 04:51:36','2021-06-17 04:51:36'),(21,'menu_items','title',2,'pt','Media','2021-06-17 04:51:37','2021-06-17 04:51:37'),(22,'menu_items','title',12,'pt','Publicações','2021-06-17 04:51:37','2021-06-17 04:51:37'),(23,'menu_items','title',3,'pt','Utilizadores','2021-06-17 04:51:37','2021-06-17 04:51:37'),(24,'menu_items','title',11,'pt','Categorias','2021-06-17 04:51:37','2021-06-17 04:51:37'),(25,'menu_items','title',13,'pt','Páginas','2021-06-17 04:51:37','2021-06-17 04:51:37'),(26,'menu_items','title',4,'pt','Funções','2021-06-17 04:51:38','2021-06-17 04:51:38'),(27,'menu_items','title',5,'pt','Ferramentas','2021-06-17 04:51:38','2021-06-17 04:51:38'),(28,'menu_items','title',6,'pt','Menus','2021-06-17 04:51:38','2021-06-17 04:51:38'),(29,'menu_items','title',7,'pt','Base de dados','2021-06-17 04:51:38','2021-06-17 04:51:38'),(30,'menu_items','title',10,'pt','Configurações','2021-06-17 04:51:38','2021-06-17 04:51:38');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png',NULL,'$2y$10$qsZBzFvCC7BD7j/t2LGaAuL798cqntO0iC6Gr4LWXnaP9.MuukHOy','mNKLOTfpvMIkbfrCtIMe0s9mN52BaxT8JV35TxEPNK0mN97btsbUM8wn3DZx',NULL,'2021-06-17 04:51:23','2021-06-17 04:51:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-06 14:29:21
